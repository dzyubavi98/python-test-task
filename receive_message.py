import pika, sys, os
import cv2
import urllib.request
import pytesseract
import numpy as np

def receive_message():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitMq'))
    channel = connection.channel()

    channel.queue_declare(queue='messages')

    def callback(ch, method, properties, body):
        try:
            url = body.decode("utf-8")
            url_response = urllib.request.urlopen(url)
            img_array = np.array(bytearray(url_response.read()), dtype=np.uint8)
            img = cv2.imdecode(img_array, -1)

            gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            gray, img_bin = cv2.threshold(gray,128,255,cv2.THRESH_BINARY | cv2.THRESH_OTSU)
            gray = cv2.bitwise_not(img_bin)

            kernel = np.ones((2, 1), np.uint8)
            img = cv2.erode(gray, kernel, iterations=1)
            img = cv2.dilate(img, kernel, iterations=1)
            out_below = pytesseract.image_to_string(img)
            print(out_below, flush=True)
        except Exception:
            print("Wrong Image", flush=True)

    channel.basic_consume(queue='messages', on_message_callback=callback, auto_ack=True)

    channel.start_consuming()

if __name__ == '__main__':
    try:
        receive()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)