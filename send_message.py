import pika

def send_message(url):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host='rabbitMq'))
    channel = connection.channel()

    channel.queue_declare(queue='messages')

    channel.basic_publish(exchange='', routing_key='messages', body=url)

    connection.close()