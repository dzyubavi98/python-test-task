from flask import Flask, jsonify
from receive_message import receive_message
from send_message import send_message

app = Flask(__name__)

request = [
	"https://www.ihdemu.com/images/blog/que-es-lorem-ipsum.jpg",
	"https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Blocksatz-Beispiel_deutsch%2C_German_text_sample_with_fully_justified_text.svg/750px-Blocksatz-Beispiel_deutsch%2C_German_text_sample_with_fully_justified_text.svg.png"
]

@app.route('/')
def main():
    for x in request:
        send_message(x)
    print(request, flush=True)    
    receive_message()
    return ""

@app.route('/receive')
def receive():   
    receive_message()
    return ""

@app.route('/send')
def send():
    for x in request:
        send_message(x)
    print(request, flush=True)    
    return jsonify(request)
    